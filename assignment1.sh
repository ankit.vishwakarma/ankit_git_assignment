#!/bin/bash
#set -x

action=$1
PARENT=$2
CHILD=$3

if [ ${action} == addDir ]
then
    echo "I'm creating $CHILD in $PARENT"
    mkdir $PARENT/$CHILD
elif [ ${action} == deleteDir ]
then
    echo "I'm deleting $CHILD in $PARENT"
    rmdir $PARENT/$CHILD
elif [ ${action} == listFiles ]
then
    echo "I'm listing files in $PARENT"
    ls -l $PARENT | grep ^-
elif [ ${action} == listDirs ]
then
    echo "I'm listing directories in $PARENT"
    ls -l $PARENT | grep ^d
elif [ ${action} == listAll ]
then
    echo "I'm listing all files & directories in $PARENT"
    ls -la $PARENT

elif [ ${action} == addFile ]
then
    FILE=$3
    CONTENT=$4
    echo "I'm creating $FILE in $PARENT/$CHILD"
    touch $PARENT/$CHILD
    
elif [ ${action} == addContentToFile ]
then
    FILE=$3
    CONTENT=$4
    echo "I'm adding $CONTENT in $PARENT/$FILE"
    echo "$CONTENT" >> "$PARENT/$FILE"

elif [ ${action} == addContentToFileBegining ]
then
    FILE=$3
    CONTENT=$4
    echo "I'm adding $CONTENT to $FILE in the begining"
    echo $CONTENT > file2.txt
    cat $PARENT/$FILE >> file2.txt
    rm -rf $PARENT/$FILE
    mv file2.txt $PARENT/$FILE

elif [ ${action} == showFileBeginingContent ]
then
    FILE=$3
    CONTENT=$4
    echo "I'm showing file begining $CONTENT in $PARENT/$FILE"
    cat -n $PARENT/$FILE | head -n1
elif [ ${action} == showFileEndContent ]
then
    FILE=$3
    CONTENT=$4
    echo "I'm showing file end $CONTENT in $PARENT/$FILE"
    cat -n $PARENT/$FILE | tail -n1
elif [ ${action} == showFileContentAtLine ]
then
    FILE=$3
    CONTENT=$4
    echo "I'm showing file $CONTENT at line in $PARENT/$FILE"
    cat -n $PARENT/$FILE | head -n10 | tail -n1

elif [ ${action} == showFileContentForLineRange ]
then
    FILE=$3
    CONTENT=$4
    echo "I'm showing file $CONTENT for a line range in $PARENT/$FILE"
    cat -n $PARENT/$FILE | head -n10 | tail -n5

elif [ ${action} == moveFile ]
then
    FILE=$3
    echo "I'm moving $PARENT to $FILE"
    mv $PARENT $FILE

elif [ ${action} == moveFile ]
then
    FILE=$3
    echo "I'm moving $PARENT to $FILE"
    mv $PARENT $FILE

elif [ ${action} == copyFile ]
then
    FILE=$3
    echo "I'm copying $PARENT to $FILE"
    cp $PARENT $FILE

elif [ ${action} == copyFile ]
then
    FILE=$3
    echo "I'm copying $PARENT to $FILE"
    cp $PARENT $FILE

elif [ ${action} == clearFileContent ]
then
    FILE=$3
    echo "I'm clearing $FILE content"
    cat /dev/null >$FILE
elif [ ${action} == deleteFile ]
then
    FILE=$3
    echo "I'm deleting $FILE in $PARENT"
    rm -rf $PARENT/$FILE

fi

